/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// function Tube(radius, length) {
//   return Math.PI * radius ** 2 * length;
// }

function Cube(width) {
  return width ** 3;
}

// function inputRadius() {
//   rl.question(`Radius of Tube: `, (radius) => {
//     if (!isNaN(radius) && radius > 0) {
//       inputLength(radius);
//     } else if (radius < 0) {
//       console.log(`Radius must be a positive number`);
//       inputRadius();
//     } else {
//       console.log(`Radius must be a number \n`);
//       inputRadius();
//     }
//   });
// }

// function inputLength(radius) {
//   rl.question(`Length of Tube: `, (length) => {
//     if (!isNaN(length) && length > 0) {
//       console.log(`Volume of Tube : ${Tube(radius, length)} meter`);
//       inputWidth();
//     } else if (length < 0) {
//       console.log(`Length must be a positive number`);
//       inputLength(radius, length);
//     } else {
//       console.log(`Length must be a number`);
//       inputLength(radius, length);
//     }
//   });
// }

function inputWidth() {
  rl.question(`Width of Cube: `, (width) => {
    if (!isNaN(width) && width > 0) {
      console.log(`Volume of Cube : ${Cube(width)} meter`);
      rl.close();
    } else if (width < 0) {
      console.log(`Width must be a Positive number`);
      inputWidth(width);
    } else {
      console.log(`Width must be a number`);
      inputWidth(width);
    }
  });
}

console.log(`WELCOME !`);
console.log(`Cube Volume Calculator (Meter)! `);

inputWidth();
