/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate Tube volume
function Tube(radius, length) {
  return Math.PI * radius ** 2 * length;
}

/* Way 1 */
// Function for inputing length of beam
function inputRadius() {
  rl.question(`Radius: `, (radius) => {
    if (!isNaN(radius)) {
      inputWidth(radius);
    }
    elseif;
    {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

// Function for inputing width of beam
// function inputWidth(length) {
//   rl.question(`Width: `, (width) => {
//     if (!isNaN(width)) {
//       inputHeight(length, width);
//     } else {
//       console.log(`Width must be a number\n`);
//       inputWidth(length);
//     }
//   });
// }

// Function for inputing height of beam
function inputLength(radius, length) {
  rl.question(`Length: `, (length) => {
    if (!isNaN(length)) {
      console.log(`\nCube: ${beam(length, width, height)}`);
      rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(length, width);
    }
  });
}
/* End Way 1 */

/* Alternative Way */
// All input just in one code
// function input() {
//   rl.question("Length: ", function (length) {
//     length++;
//     console.log(length);
//     rl.question("Width: ", (width) => {
//       rl.question("Height: ", (height) => {
//         if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
//           console.log(`\nBeam: ${beam(length, width, height)}`);
//           rl.close();
//         } else {
//           console.log(`Length, Width and Height must be a number\n`);
//           input();
//         }
//       });
//     });
//   });
// }
/* End Alternative Way */

console.log(`Rectangle`);
console.log(`=========`);
// inputLength(); // Call way 1
input(); // Call Alternative Way
