const vegetableFruit = ["tomato", "broccoli", "kale", "cabbage", "apple"];

for (let i = 0; i < vegetableFruit.length; i++) {
  if (vegetableFruit[i] !== "apple")
    console.log(
      `${vegetableFruit[i]} is a healthy food, it's definitely worth to eat. `
    );
}
