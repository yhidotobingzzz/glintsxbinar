const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const dataDiri = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Timo",
    status: "Negative",
  },
  {
    name: "Andi",
    status: "Suspect",
  },
  {
    name: "Rudy",
    status: "Positive",
  },
];

function intro() {
  console.log("Whats The status do You want?");
  console.log("'P' = Positive");
  console.log("'N' = Negative");
  console.log("'S' = Suspect");
  console.log("'E' = Exit");
  console.log();
}

function choiceStatus() {
  rl.question(`Write down: `, (pilihan) => {
    switch (pilihan) {
      case "P":
      case "p":
        dataDiri.filter((item) => {
          if (item.status === "Positive") {
            console.log(`${item.name} is Positive`);
            rl.close();
          }
        });
        break;
      case "N":
      case "n":
        dataDiri.filter((item) => {
          if (item.status === "Negative") {
            console.log(`${item.name} is Negative`);
            rl.close();
          }
        });
        break;
      case "S":
      case "s":
        dataDiri.filter((item) => {
          if (item.status === "Suspect") {
            console.log(`${item.name} is Suspect`);
            rl.close();
          }
        });
        break;
      case "E":
      case "e":
        rl.close();

        break;

      default:
        console.log("Try Again ");
        choiceStatus();
        break;
    }
  });
}

module.exports = { intro, choiceStatus };
