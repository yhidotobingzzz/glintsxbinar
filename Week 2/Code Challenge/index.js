const data = require("./lib/arrayFactory");
const test = require("./lib/test");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

function sortAscending(data) {
  // Code Here
  let cleaner = clean(data);

  for (var i = 0; i < cleaner.length; i++) {
    for (var j = 0; j < cleaner.length; j++) {
      if (cleaner[j] > cleaner[j + 1]) {
        // swap data[j], data[j+1]
        var temp = cleaner[j];
        cleaner[j] = cleaner[j + 1];
        cleaner[j + 1] = temp;
      }
    }
  }

  return cleaner;
}

function sortDecending(data) {
  // Code Here
  let cleaner = clean(data);
  for (var i = 0; i < cleaner.length; i++) {
    for (var j = 0; j < cleaner.length; j++) {
      if (cleaner[j] < cleaner[j + 1]) {
        // swap data[j], data[j+1]
        var temp = cleaner[j];
        cleaner[j] = cleaner[j + 1];
        cleaner[j + 1] = temp;
      }
    }
  }

  return cleaner;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
