const warehouse = ["Brick", "Cement", "Steel", "Betonizer"];

function designTheHouse() {
  console.log("Design the House");
}

function findTheBuilder() {
  console.log("Find the builder");
}

function goToWarehouse() {
  console.log("Go To Warehouse");
}

function checkTheSand() {
  if (warehouse.includes("Sand")) {
    console.log("The sand available");
    return true;
  }
  console.log("The sand is not available");
  return false;
}

function goToOtherWarehouse(sand) {
  if (!sand) {
    console.log("Go to other warehouse");
    console.log("Buy the sand");
    console.log("Go back to the site");
  }
}

function buildTheFoundation() {
  console.log("Ordered the builder build the foundation");
}

function buildTheWallAndTheRoof() {
  console.log("Ordered the builder build the wall and the roof");
}

function buildTheInterior() {
  console.log("Ordered the builder bulid the interior");
}

function findTheOtherBuilder(number) {
  if (number > 14) {
    console.log("Find another builder");
  }
}

function houseIsReady() {
  console.log("House is Ready");
}

function start() {
  designTheHouse();
  findTheBuilder();
  const sand = checkTheSand();
  goToOtherWarehouse(sand);
  buildTheFoundation();
  buildTheWallAndTheRoof();
  findTheOtherBuilder(15);
  buildTheInterior();
  houseIsReady();
}
start();
