const car = require("../models/car.json");

class CarController {
  getAllcars(req, res) {
    try {
      res.status(200).json({
        data: car,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  getOneCar(req, res) {
    try {
      const carx = car.filter((data) => data.id == req.params.id);
      res.status(200).json({
        data: carx,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  createCar(req, res) {
    try {
      car.push(req.body);

      res.status(200).json({
        data: req.body,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  updateCar(req, res) {
    try {
      const update = car.find((data) => {
        if (data.id == req.params.id) {
          data.name = req.body.name;
          data.engine = req.body.engine;
          data.assembly = req.body.assembly;
        }
      });
      res.status(200).json({
        data: req.body,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  deleteCar(req, res) {
    try {
      const find = car.find((data) => data.id == req.params.id);
      const index = car.indexOf(find);
      if (index > -1) {
        car.splice(index, 1);
      }
      res.status(200).json({
        message: "Sucessfully delete",
        data: car,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new CarController();
