const route = require("express").Router();

const {
  getAllcars,
  getOneCar,
  createCar,
  updateCar,
  deleteCar,
} = require("../controllers/carController");

route.get("/", getAllcars);
route.get("/:id", getOneCar);

route.post("/", createCar);

route.put("/:id", updateCar);

route.delete("/:id", deleteCar);

module.exports = route;
