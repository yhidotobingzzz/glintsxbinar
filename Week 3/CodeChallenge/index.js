const express = require("express");

const app = express();

const carRoutes = require("./routes/carroutes");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use("/", carRoutes);
app.listen(3000, () => console.log(`Server running on port 3000`));
