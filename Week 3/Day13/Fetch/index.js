const fetch = require("node-fetch");
const url = "https://jsonplaceholder.typicode.com/posts";
const url2 = "https://jsonplaceholder.typicode.com/albums";
const url3 = "https://jsonplaceholder.typicode.com/users";

async function fetchAPI() {
  try {
    fetch(url)
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
}
async function fetchAPI2() {
  try {
    fetch(url2)
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
}
async function fetchAPI() {
  try {
    fetch(url3)
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
}

fetchAPI();
fetchAPI2();
fetchAPI3();
