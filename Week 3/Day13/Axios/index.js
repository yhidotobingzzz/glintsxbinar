const axios = require("axios");
const url = "https://jsonplaceholder.typicode.com/posts";
const url2 = "https://jsonplaceholder.typicode.com/albums";
const url3 = "https://jsonplaceholder.typicode.com/users";

async function fetch() {
  try {
    axios
      .get(url)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch2() {
  try {
    axios
      .get(url2)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch3() {
  try {
    axios
      .get(url3)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
fetch();
fetch2();
fetch3();
