// Make class Person
class Animal {
  // // Static property
  // static isLife = true;

  // Instance property
  constructor(type, diet, genus) {
    this.type = type;
    this.diet = diet;
    this.genus = genus;
  }
}

module.exports = Animal;
