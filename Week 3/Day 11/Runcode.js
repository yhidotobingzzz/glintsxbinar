const Animal = require("./Animal");
const Panther = require("./Panther");
const Lion = require("./Lion");

let lion = new Lion();
console.log(lion.characteristic());

let panther = new Panther();
console.log(panther.characteristic());
