const Animal = require("./Animal");

class Lion extends Animal {
  constructor() {
    super("Mammals", "Carnivora", "Panthera");
    this.animal = "Lion";
    this.hunt = "Group";
    this.makenoise = "Roar";
  }
  characteristic() {
    return ` Animal  : ${this.animal}\n Type    : ${this.type}\n Diet    : ${this.diet}\n Genus   : ${this.genus}\n Noise   : ${this.makenoise}\n Hunt    : ${this.hunt}\n`;
  }
}
module.exports = Lion;
