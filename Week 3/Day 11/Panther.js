const Animal = require("./Animal");

class Panther extends Animal {
  constructor() {
    super("Mammals", "Carnivora", "Panthera");
    this.animal = "Panther";
    this.hunt = "Solo";
    this.makenoise = "Groam";
  }
  characteristic() {
    return ` Animal  : ${this.animal}\n Type    : ${this.type}\n Diet    : ${this.diet}\n Genus   : ${this.genus}\n Noise   : ${this.makenoise}\n Hunt    : ${this.hunt}\n`;
  }
}

module.exports = Panther;
