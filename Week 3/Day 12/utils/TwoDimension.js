const Geometry = require("./Geometry");

class TwoDimension extends Geometry {
  static count = 0;
  constructor(name) {
    super(name, "Two Dimension");

    if (this.constructor === TwoDimension) {
      throw new Error("TwoDimension is an abstract class.");
    }
    TwoDimension.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.type}`);
  }

  calculateCircumference() {
    console.log(`${this.name} Circumference\n=====================`);
  }

  calculateArea() {
    console.log(`${this.name} Area\n=====================`);
  }
}

module.exports = TwoDimension;
