const ThreeDimension = require("./ThreeDimension");

class Beam extends ThreeDimension {
  static count = 0;
  constructor(breadth, length, thickness) {
    super("Beam");
    this.breadth = breadth;
    this.length = length;
    this.thickness = thickness;
    Beam.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateSurfaceArea(name) {
    super.calculateSurfaceArea();
    const result =
      2 *
      (this.length * this.breadth +
        this.length * this.thickness +
        this.breadth * this.thickness);
    console.log(`${name} is trying to calculate Surface: ${result}\n`);
    return result;
  }

  calculateVolume(name) {
    super.calculateVolume();
    const result = this.length * this.breadth * this.thickness;
    console.log(`${name} is trying to calculate Volume: ${result}\n`);
    return result;
  }
}

module.exports = Beam;
