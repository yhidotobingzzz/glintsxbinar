class Geometry {
  static count = 0;
  constructor(name, type) {
    if (this.constructor === Geometry) {
      throw new Error("Geometry is an abstract class.");
    }
    this.name = name;
    this.type = type;
    Geometry.count++;
  }

  whoAmI() {
    console.log("Who Am I?");
  }
}

module.exports = Geometry;
