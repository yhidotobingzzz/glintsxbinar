const TwoDimension = require("./TwoDimension");

class Rectangle extends TwoDimension {
  static count = 0;
  constructor(width, length) {
    super("Rectangle");
    this.width = width;
    this.length = length;
    Rectangle.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateCircumference(name) {
    super.calculateCircumference();
    const result = 2 * (this.width + this.length);
    console.log(
      `${name} is trying to calculate Circumference of Rectangle: ${result}\n`
    );
    return result;
  }

  calculateArea(name) {
    super.calculateArea();
    const result = this.width * this.length;
    console.log(
      `${name} is trying to calculate Area of Rectangle: ${result}\n`
    );
    return result;
  }
}

module.exports = Rectangle;
