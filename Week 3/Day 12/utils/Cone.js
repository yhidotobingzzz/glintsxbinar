const ThreeDimension = require("./ThreeDimension");

class Cone extends ThreeDimension {
  static count = 0;
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
    Cone.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateSurfaceArea(name) {
    super.calculateSurfaceArea();
    const slant = Math.sqrt(this.radius ** 2 + this.height ** 2);
    const result = Math.round(Math.PI * this.radius * (this.radius + slant));
    console.log(`${name} is trying to calculate Surface Area: ${result}\n`);
    return result;
  }

  calculateVolume(name) {
    super.calculateVolume();
    const result = Math.round((Math.PI * this.radius ** 2 * this.height) / 3);
    console.log(`${name} is trying to calculate Volume: ${result}\n`);
    return result;
  }
}

module.exports = Cone;
