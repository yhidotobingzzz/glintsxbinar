const ThreeDimension = require("./ThreeDimension");

class Tube extends ThreeDimension {
  static count = 0;

  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
    Tube.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateSurfaceArea(name) {
    super.calculateSurfaceArea();
    const result = Math.round(2 * Math.PI * this.radius * this.height);
    console.log(`${name} is trying to calculate Surface Area: ${result}\n`);
    return result;
  }

  calculateVolume(name) {
    super.calculateVolume();
    const result = Math.round(Math.PI * this.radius ** 2 * this.height);
    console.log(`${name} is trying to calculate Volume: ${result}\n`);
    return result;
  }
}

module.exports = Tube;
