const TwoDimension = require("./TwoDimension");

class Triangle extends TwoDimension {
  static count = 0;
  constructor(base, height, aside, bside, cside) {
    super("Triangle");
    this.base = base;
    this.height = height;
    this.aside = aside;
    this.bside = bside;
    this.cside = cside;
    Triangle.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateCircumference(name) {
    super.calculateCircumference();
    const result = this.aside + this.bside + this.cside;

    console.log(
      `${name} is trying to calculate Circumference of Triangle: ${result}\n`
    );
    return result;
  }

  calculateArea(name) {
    super.calculateArea();
    const result = (1 / 2) * this.base * this.height;
    console.log(`${name} is trying to calculate Area of Triangle: ${result}\n`);
    return result;
  }
}

module.exports = Triangle;
