const Geometry = require("./Geometry");

class ThreeDimension extends Geometry {
  static count = 0;
  constructor(name) {
    super(name, "Three Dimension");

    if (this.constructor === ThreeDimension) {
      throw new Error("ThreeDimension is an abstract class.");
    }
    ThreeDimension.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.type}`);
  }

  calculateSurfaceArea() {
    console.log(`${this.name} Surface Area\n=====================`);
  }

  calculateVolume() {
    console.log(`${this.name} Volume\n=====================`);
  }
}

module.exports = ThreeDimension;
