const ThreeDimension = require("./ThreeDimension");

class Cube extends ThreeDimension {
  static count = 0;
  constructor(edge) {
    super("Cube");
    this.edge = edge;
    Cube.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateSurfaceArea(name) {
    super.calculateSurfaceArea();
    const result = 6 * this.edge * this.edge;
    console.log(`${name} is trying to calculate Surface: ${result}\n`);
    return result;
  }

  calculateVolume(name) {
    super.calculateVolume();
    const result = this.edge ** 3;
    console.log(`${name} is trying to calculate Volume: ${result}\n`);
    return result;
  }
}

module.exports = Cube;
