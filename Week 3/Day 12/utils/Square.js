const TwoDimension = require("./TwoDimension");

class Square extends TwoDimension {
  static count = 0;
  constructor(width, length) {
    super("Square");
    this.width = width;
    this.length = length;
    Square.count++;
  }

  // Method override
  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name}`);
  }

  // Method overload
  calculateCircumference(name) {
    super.calculateCircumference();
    const result = 2 * (this.width + this.length);
    console.log(`\n${name} is trying to calculate Circumference: ${result}`);
    return result;
  }

  calculateArea(name) {
    super.calculateArea();
    const result = this.width * this.length;
    console.log(`\n${name} is trying to calculate Area: ${result}`);
    return result;
  }
}

module.exports = Square;
