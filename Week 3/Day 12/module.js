// Import modules
const Rectangle = require("./utils/Rectangle");
const Triangle = require("./utils/Triangle");
const Beam = require("./utils/Beam");
const Cube = require("./utils/Cube");
const Tube = require("./utils/Tube");
const Cone = require("./utils/Cone");

// Import modules for abstract classes
const Geometry = require("./utils/Geometry");
const TwoDimension = require("./utils/TwoDimension");
const ThreeDimension = require("./utils/ThreeDimension");

// Instantiate Mas Yhido's rectangle and triangle
const rect1 = new Rectangle(
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1)
);
const tri1 = new Triangle(
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1)
);

// Run overloaded methods
const rectangleCirc = rect1.calculateCircumference("Adib");
const triangleCirc = tri1.calculateCircumference("Amri");
const rectangleArea = rect1.calculateArea("Amri");
const triangleArea = tri1.calculateArea("Adib");

// Show output
console.log(`\nThe total circumference is ${rectangleCirc + triangleCirc}\n`);
console.log(`\nThe total area is ${rectangleArea + triangleArea}\n`);
console.log(`


`);

// Instantiate Mas Amri's beam and cube
const beam1 = new Beam(
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1)
);
const cube1 = new Cube(Math.floor(Math.random() * 20 + 1));

// Run overloaded methods
const beamSurfArea = beam1.calculateSurfaceArea("Yhido");
const cubeSurfArea = cube1.calculateSurfaceArea("Adib");
const beamVolume = beam1.calculateVolume("Adib");
const cubeVolume = cube1.calculateVolume("Yhido");

// Instantiate Mas Adib's tube and cone
const tube1 = new Tube(
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1)
);
const cone1 = new Cone(
  Math.floor(Math.random() * 20 + 1),
  Math.floor(Math.random() * 20 + 1)
);

// Run overloaded methods
const tubeSurfArea = tube1.calculateSurfaceArea("Amri");
const coneSurfArea = cone1.calculateSurfaceArea("Yhido");
const tubeVolume = tube1.calculateVolume("Yhido");
const coneVolume = cone1.calculateVolume("Amri");

// Show output
console.log(
  `\nThe total surface area is ${
    beamSurfArea + cubeSurfArea + tubeSurfArea + coneSurfArea
  }\n`
);
console.log(
  `\nThe total volume is ${beamVolume + cubeVolume + tubeVolume + coneVolume}\n`
);
console.log(`


`);

// Count instances of the superclasses
console.log(`Number of Geometry instances: ${Geometry.count}`);
console.log(`Number of TwoDimension instances: ${TwoDimension.count}`);
console.log(`Number of ThreeDimension instances: ${ThreeDimension.count}`);
console.log(`


`);

// Try to directly instantiate abstract superclasses
try {
  geo1 = new Geometry("Square", "Two Dimension");
} catch (error) {
  //   console.log(error); too long but it works
  console.log("ERROR!!!");
}

try {
  twoD1 = new TwoDimension("Triangle");
} catch (error) {
  //   console.log(error); too long but it works
  console.log("ERROR!!!");
}

try {
  threeD1 = new ThreeDimension("Cone");
} catch (error) {
  //   console.log(error); too long but it works
  console.log("ERROR!!!");
}
