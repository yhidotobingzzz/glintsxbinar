const readline = require("readline");
const rumus = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function calculateVolumeTubeInM(radius, length) {
  console.log("Volume Tube :", (3.14 * radius ** 2 * length) / 1000, "meter");
}

function calculateVolumeTubeInMM(radius, length) {
  console.log("Volume Tube :", 3.14 * radius ** 2 * length, "milimeter");
}

function calculateVolumeCubeInMM(width) {
  console.log("Volume Cube :", width ** 3, "milimeter");
}

function calculateVolumeCubeInM(width) {
  console.log("Volume Cube :", width ** 3 / 1000, "meter");
}
console.log("Calculate Volume Tube and Volume Cube");

rumus.question("Radius of Tube in mm ? ", (radius) => {
  rumus.question("length of Tube in mm ?", (length) => {
    rumus.question("Width of Cube in mm ?", (width) => {
      calculateVolumeTubeInM(radius, length);
      calculateVolumeTubeInMM(radius, length);
      calculateVolumeCubeInMM(width);
      calculateVolumeCubeInM(width);

      rumus.close();
    });
  });
});
